var amqp = require('amqplib/callback_api');
var nodemailer = require('nodemailer');

amqp.connect('amqp://rabbitmq', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'mailing';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, function(msg) {
            console.log(" [x] Received %s", msg.content.toString());

            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                  user: 'neighborrescue56@gmail.com',
                  pass: 'neighborrescue420'
                }
              });
              
              var rcvEmail = msg.content.toString(); 

              var mailOptions = {
                from: 'neighborrescue56@gmail.com',
                to: rcvEmail,
                subject: 'Trip accepted.',
                text: 'Your request was accepted. Check upcoming trips.'
              };
              
              transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });
        }, {
            noAck: true
        });
    });
});